export default [
  {
    name: 'The Avengers',
    year: '2012',
    image: { url: 'https://i.ytimg.com/vi/48fKIXlxaXk/maxresdefault.jpg' },
    description: 'Cuando Loki llega a la tierra se roba el Teseracto e hipnotiza a Clint Barton, al Dr. Selvig y a algunos agentes de S.H.I.E.L.D. Para enfrentar esta amenaza, Nick Fury reúne a Capitán América, Iron Man, Thor, Hulk, Black Widow y Hawkeye siendo liberado, generando así al famoso equipo conocido como Los Vengadores.          ',
    colors: ["#b1b29f", "#1b7e9b", "#104059" ],
    characters: [
      {
        name: 'Robert Downey Jr',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/220px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg'},
        character: 'Tony Stark / Iron Man'
      },
      {
        name: 'Chris Evans',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg/220px-5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg'},
        character: 'Steve Rogers / Capitán América'
      },
      {
        name: 'Mark Ruffalo',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mark_Ruffalo_June_2014.jpg/250px-Mark_Ruffalo_June_2014.jpg'},
        character: 'Bruce Banner / Hulk'
      },
      {
        name: 'Chris Hemsworth',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg/220px-Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg'},
        character: 'Thor'
      },
      {
        name: 'Scarlett Johansson',
        image: { url: 'http://es.web.img2.acsta.net/pictures/19/03/14/11/10/0992674.jpg'},
        character: 'Natasha Romanoff / Viuda Negra'
      },
      {
        name: 'Jeremy Renner',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Jeremy_Renner_%288426813772%29.jpg/220px-Jeremy_Renner_%288426813772%29.jpg'},
        character: 'Clint Barton / Ojo de Halcón'
      },
      {
        name: 'Tom Hiddleston',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Tom_Hiddleston_by_Gage_Skidmore_3.jpg/220px-Tom_Hiddleston_by_Gage_Skidmore_3.jpg'},
        character: 'Loki'
      },
      {
        name: 'Samuel L. Jackson',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Samuel_L._Jackson_2019_by_Glenn_Francis.jpg/220px-Samuel_L._Jackson_2019_by_Glenn_Francis.jpg'},
        character: 'Nick Fury:'
      }
    ]
  },
  {
    name: 'Avengers: Age of Ultron',
    year: '2015',
    image: { url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxLahmPR9J8ysN-2SlDPa___-Y1g9RW0871UQYrwpSxNsFFUKo' },
    description: 'Con S.H.I.E.L.D. destruido y los Vengadores necesitados de un descanso en su deber como héroes, Tony Stark inicia un programa inactivo con el objetivo de mantener la paz mundial, creando a un robot de alta inteligencia llamado Ultrón, para detectar amenazas globales. Las cosas comienzan a complicarse cuando Ultron obtiene una mente y conciencia propia y decide erradicar a toda la humanidad por considerarla el mayor riesgo para el planeta, por lo que los héroes más poderosos del universo, incluyendo a Iron Man, Capitán América, Thor, Hulk, Black Widow y Hawkeye, serán expuestos ante la prueba más difícil que han hecho hasta ahora cuando el destino del planeta se ponga en juego. Cuando Ultrón emerja, dependerá de Los Vengadores detener sus terribles planes, y pronto incómodas alianzas e inesperadas acciones pavimentarán el camino para una épica y única aventura global.1​ Además cuenta con la aparición de War Machine, Falcón, Quicksilver, Scarlet Witch y el debut de Visión.',
    colors: ["#728592", '#162742', '#3b5168'],
    characters: [
      {
        name: 'Robert Downey Jr',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/220px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg'},
        character: 'Tony Stark / Iron Man'
      },
      {
        name: 'Chris Evans',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg/220px-5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg'},
        character: 'Steve Rogers / Capitán América'
      },
      {
        name: 'Mark Ruffalo',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mark_Ruffalo_June_2014.jpg/250px-Mark_Ruffalo_June_2014.jpg'},
        character: 'Bruce Banner / Hulk'
      },
      {
        name: 'Chris Hemsworth',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg/220px-Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg'},
        character: 'Thor'
      },
      {
        name: 'Scarlett Johansson',
        image: { url: 'http://es.web.img2.acsta.net/pictures/19/03/14/11/10/0992674.jpg'},
        character: 'Natasha Romanoff / Viuda Negra'
      },
      {
        name: 'Jeremy Renner',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Jeremy_Renner_%288426813772%29.jpg/220px-Jeremy_Renner_%288426813772%29.jpg'},
        character: 'Clint Barton / Ojo de Halcón'
      },
      {
        name: 'Tom Hiddleston',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Tom_Hiddleston_by_Gage_Skidmore_3.jpg/220px-Tom_Hiddleston_by_Gage_Skidmore_3.jpg'},
        character: 'Loki'
      },
      {
        name: 'Samuel L. Jackson',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Samuel_L._Jackson_2019_by_Glenn_Francis.jpg/220px-Samuel_L._Jackson_2019_by_Glenn_Francis.jpg'},
        character: 'Nick Fury:'
      }
    ]
  },
  {
    name: 'Avengers: Infinity War',
    year: '2018',
    image: { url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRKssc1dLNJ2PXuDKZA77ulquGUnyWkzQcab_tYYMfVoT8nycs2' },
    description: 'Los Vengadores y sus aliados continúan protegiendo al mundo de amenazas demasiado grandes para que un solo héroe las pueda manejar, un nuevo peligro emerge de entre las sombras cósmicas: Thanos. Como déspota de la infamia intergaláctica, su meta es recolectar las seis Gemas del Infinito, artefactos de inimaginable poder, y usarlas para imponer sus retorcidos deseos en toda la realidad. Todo para lo que los Vengadores han luchado les ha llevado a este momento, el destino de la Tierra y la propia existencia nunca han sido más inciertos. Además cuenta con la aparición de Star-Lord, Rocket Raccoon, Gamora, Drax, Groot, Black Panther, Mantis, Winter Soldier, Spider-Man, Doctor Strange, Wong y Nebula.',
    colors: ["#d98a4e", "#c02214", "#611822" ],
    characters: [
      {
        name: 'Robert Downey Jr',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/220px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg'},
        character: 'Tony Stark / Iron Man'
      },
      {
        name: 'Chris Evans',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg/220px-5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg'},
        character: 'Steve Rogers / Capitán América'
      },
      {
        name: 'Mark Ruffalo',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mark_Ruffalo_June_2014.jpg/250px-Mark_Ruffalo_June_2014.jpg'},
        character: 'Bruce Banner / Hulk'
      },
      {
        name: 'Chris Hemsworth',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg/220px-Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg'},
        character: 'Thor'
      },
      {
        name: 'Scarlett Johansson',
        image: { url: 'http://es.web.img2.acsta.net/pictures/19/03/14/11/10/0992674.jpg'},
        character: 'Natasha Romanoff / Viuda Negra'
      },
      {
        name: 'Jeremy Renner',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Jeremy_Renner_%288426813772%29.jpg/220px-Jeremy_Renner_%288426813772%29.jpg'},
        character: 'Clint Barton / Ojo de Halcón'
      },
      {
        name: 'Tom Hiddleston',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Tom_Hiddleston_by_Gage_Skidmore_3.jpg/220px-Tom_Hiddleston_by_Gage_Skidmore_3.jpg'},
        character: 'Loki'
      },
      {
        name: 'Samuel L. Jackson',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Samuel_L._Jackson_2019_by_Glenn_Francis.jpg/220px-Samuel_L._Jackson_2019_by_Glenn_Francis.jpg'},
        character: 'Nick Fury:'
      }
    ]
  },
  {
    name: 'Avengers: Endgame',
    year: '2019',
    image: { url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTKR2HrtP6R3UWxGoDA3aEKeWFRp4wYHaKmVBZ8w7QmgqQJ7rKl' },
    description: 'Tras los eventos devastadores de Avengers: Infinity War, el universo está en ruinas debido a los efectos del titán loco, Thanos. Con la ayuda de los aliados restantes, los Vengadores deben reunirse una vez más para deshacer las acciones de Thanos y restaurar el orden del universo de una vez y para siempre, sin importar las consecuencias que pueda tener, aunque algunos pagarán el precio. 2​ Además cuenta con la aparición de Ant-Man, Wasp y Capitana Marvel.',
    colors: ["#9c2e5e", "#2a10d8", "#150375" ],
    characters: [
      {
        name: 'Robert Downey Jr',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/220px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg'},
        character: 'Tony Stark / Iron Man'
      },
      {
        name: 'Chris Evans',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg/220px-5_Dec._2016_CJCS_USO_Holiday_Tour_-_Incirlik_Air_Base_161205-D-PB383-044_%2831430825446%29_%28cropped%29_%28cropped%29.jpg'},
        character: 'Steve Rogers / Capitán América'
      },
      {
        name: 'Mark Ruffalo',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mark_Ruffalo_June_2014.jpg/250px-Mark_Ruffalo_June_2014.jpg'},
        character: 'Bruce Banner / Hulk'
      },
      {
        name: 'Chris Hemsworth',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg/220px-Chris_Hemsworth_by_Gage_Skidmore_2_%28cropped%29.jpg'},
        character: 'Thor'
      },
      {
        name: 'Scarlett Johansson',
        image: { url: 'http://es.web.img2.acsta.net/pictures/19/03/14/11/10/0992674.jpg'},
        character: 'Natasha Romanoff / Viuda Negra'
      },
      {
        name: 'Jeremy Renner',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Jeremy_Renner_%288426813772%29.jpg/220px-Jeremy_Renner_%288426813772%29.jpg'},
        character: 'Clint Barton / Ojo de Halcón'
      },
      {
        name: 'Tom Hiddleston',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Tom_Hiddleston_by_Gage_Skidmore_3.jpg/220px-Tom_Hiddleston_by_Gage_Skidmore_3.jpg'},
        character: 'Loki'
      },
      {
        name: 'Samuel L. Jackson',
        image: { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Samuel_L._Jackson_2019_by_Glenn_Francis.jpg/220px-Samuel_L._Jackson_2019_by_Glenn_Francis.jpg'},
        character: 'Nick Fury:'
      }
    ]
  },
];
