import React from 'react';
import {
  FlatList,
  ScrollView,
} from 'react-native';
import {
  Subtitle,
  ImageBackground,
  GridRow,
  Tile,
  Text,
  View,
  Heading,
  Overlay,
  Button,
  Lightbox,
} from '@shoutem/ui';

import {
  Parallax,
  HeroHeader,
  FadeOut,
  FadeIn,
  ScrollDriver,
} from '@shoutem/animation';

import Constants from 'expo-constants';
import { LinearGradient } from 'expo-linear-gradient';
import avengersData from './avengersData'

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      avengersData,
    };
  }


  renderRow = ({ item }) => {
    const { navigation } = this.props;
    const { open } = this.state;
    const cellViews = item.map((restaurant) => (
      <Lightbox
        onOpen={() => this.setState({ open: true})}
        onClose={() => this.setState({ open: false})}
        key={`${restaurant.name}`}
      >
        <ImageBackground
          styleName="featured"
          source={{ uri: restaurant.image.url }}
        >
          {!open && (
            <Tile>
            <Overlay>
              <Heading>{restaurant.name}</Heading>
              <Subtitle>{restaurant.year}</Subtitle>
            </Overlay>
            <Button onPress={() => navigation.navigate('Details', { item })}>
              <Text>VER MÁS</Text>
            </Button>
          </Tile>)}
        </ImageBackground>
      </Lightbox>
    ));

    return (
      <GridRow columns={1}>
        {cellViews}
      </GridRow>
    );
  }

  render() {
    const { avengersData } = this.state;
    const groupedData = GridRow.groupByRows(avengersData, 1);

    const driver = new ScrollDriver();
    return (
      <ScrollView {...driver.scrollViewProps}>
        <HeroHeader driver={driver} style={{ marginTop: Constants.statusBarHeight }}>
          <ImageBackground
            styleName="large-banner"
            source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTXDUhI_BSZoShnEPPhplkEGTrcBEWmyqmR87TBYcsKIb0T_xrF' }}
            style={{ paddingBottom: 30 }}
          >
            <Tile>
              <Parallax driver={driver} scrollSpeed={1.2} header>
                <FadeIn inputRange={[-20, 0]} driver={driver}>
                  <FadeOut inputRange={[100, 150]} driver={driver}>
                    <Heading>TETRALOGÍA DE AVENGERS</Heading>
                    <Subtitle> Marvel Cinematic Universe</Subtitle>
                  </FadeOut>
                </FadeIn>
              </Parallax>
            </Tile>
          </ImageBackground>
        </HeroHeader>
        <LinearGradient
          colors={['#932d54', '#1205b6', '#682ec3', '#0d0325']}
        >
          <View
            styleName="content"
            style={{
              backgroundColor: 'trasparent',
            }}
          >
            <View style={{ paddingVertical: 15, paddingHorizontal: 5 }}>
              <Text style={{ color: 'white', fontSize: 18, textAlign: 'justify' }}>
                La tetralogía de Los Vengadores es una franquicia de cuatro películas
                que se inició en el año 2012 con la película The Avengers y finalizó
                con Avengers: Endgame que se estrenó en 2019.
              </Text>
              <Text style={{
                marginTop: 20, color: 'white', fontSize: 18, textAlign: 'justify',
              }}
              >
                Por ahora, la saga ha recaudado más de 7000 millones de dólares,
                con un presupuesto de unos 770 millones de dólares.
                Todas las películas están basadas en historias publicadas en revistas cómics
                de Marvel.
              </Text>
            </View>
            <FlatList
              data={groupedData}
              renderItem={this.renderRow}
              keyExtractor={(item) => item[0].name}
            />
          </View>
        </LinearGradient>
      </ScrollView>
    );
  }
}
