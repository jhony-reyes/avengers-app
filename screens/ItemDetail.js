import React from 'react';
import { ScrollView, FlatList } from 'react-native';

import {
  ScrollDriver,
} from '@shoutem/animation';

import {
  Tile,
  Title,
  Text,
  Subtitle,
  View,
  ImageBackground,
  Overlay,
  Heading,
  Image,
  Row,
} from '@shoutem/ui';
import { LinearGradient } from 'expo-linear-gradient';

export default class ItemDetail extends React.Component {
    constructor(props) {
      super(props);

      this.state = {}
    }

    render() {
      const { navigation: { state: { params: { item } } } } = this.props;
      const driver = new ScrollDriver();
      return (
          <ScrollView {...driver.scrollViewProps}>
            <ImageBackground
              styleName="large-banner"
              source={{ uri: item[0].image.url }}
            >
              <Tile>
                <Overlay styleName="fill-parent image-overlay">
                  <Title styleName="sm-gutter-horizontal">{item[0].name}</Title>
                  <Subtitle>{item[0].year}</Subtitle>
                </Overlay>
              </Tile>
            </ImageBackground>
            <LinearGradient
          colors={item[0].colors}
        >
            <View
              styleName="content"
              style={{
                backgroundColor: 'transparent',
                padding: 5,
              }}
            >
              <Text style={{color: 'white'}}>
                {item[0].description}
              </Text>
              <Heading style={{color: 'white', paddingTop: 20, paddingBottom: 10}}>Reparto principal</Heading>
              <FlatList
                data={item[0].characters}
                renderItem={(data) => (
                  <Row>
                    <Image
                      styleName="small rounded-corners"
                      source={{ uri: data.item.image.url }}
                    />
                    <View>
                      <Title styleName="top">{data.item.name}</Title>
                      <Subtitle>{data.item.character}</Subtitle>
                    </View>
                  </Row>
                )}
                keyExtractor={(i) => i.name}
              ></FlatList>
            </View>
            </LinearGradient>
          </ScrollView>
      );
    }
}
