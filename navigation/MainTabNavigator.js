import React from 'react';
import { Platform, Animated, Easing } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ItemDetails from '../screens/ItemDetail';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {
    headerMode: 'none',
  },
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: {
      screen: ItemDetails,
      navigationOptions: () => ({
        gesturesEnabled: true
      }),
    },
  },
  config,
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name="md-home"
    />
  ),
};

HomeStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config,
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Info',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused}
    name={
      Platform.OS === 'ios'
        ? `ios-information-circle${focused ? '' : '-outline'}`
        : 'md-information-circle'
    }
    />
  ),
};

SettingsStack.path = '';

const bottomNavigator = createMaterialBottomTabNavigator(
  {
    HomeStack,
    SettingsStack,
  },
  {
    activeColor: '#FFFFFF'
  }
);

export default bottomNavigator;
